const gulp = require('gulp');
const concat = require('gulp-concat');
const minifyCSS = require('gulp-csso');
const minifyJS = require('gulp-minify');
const environments = require('gulp-environments');
const development = environments.development;
const production = environments.production;
// Create a browser sync instance
const bs = require('browser-sync').create();

// Main tasks
gulp.task('default', ['development']);
gulp.task('development', ['set-dev', 'css', 'browser-sync']);
gulp.task('production', ['set-pro', 'css', 'js']);

// Subtasks
gulp.task('set-dev', function () {
  return environments.current(development);
});

gulp.task('set-pro', function () {
  return environments.current(production);
});

gulp.task('css', function () {
  return gulp.src(['src/assets/css/variables.css', 'src/assets/css/fonts.css', 'src/assets/css/normalize.css', 'src/assets/css/main.css', 'src/assets/css/*.css'])
    .pipe(concat('app.min.css'))
    .pipe(production(minifyCSS()))
    .pipe(gulp.dest('./src/dist/css'));
});

gulp.task('js', function () {
  return gulp.src(['src/index.js', 'src/app/app.router.js', 'src/app/*/*.js'])
    .pipe(concat('app.js'))
    .pipe(minifyJS({
      ext: {
        min: '.min.js'
      }
    }))
    .pipe(gulp.dest('./src/dist/js'));
});

gulp.task('browser-sync', function () {
  bs.init({
    server: {
      baseDir: './src'
    }
  });
});