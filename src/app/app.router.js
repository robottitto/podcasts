import { EpisodeComponent } from './controllers/episode.component.js';
import { PodcastComponent } from './controllers/podcast.component.js';
import { PodcastsComponent } from './controllers/podcasts.component.js';
import { ViewPipe } from './pipes/view.pipe.js';

let episodeComponent = new EpisodeComponent();
let podcastComponent = new PodcastComponent();
let podcastsComponent = new PodcastsComponent();
let viewPipe = new ViewPipe();

// Events
window.onpopstate = function (event) {
  if (event.state) {
    console.log(event.state);
    switch (event.state.name) {
      case 'podcasts-list':
        viewPipe.loadView('/app/views/podcasts.component.html', ViewPipe.VIEW_MAIN, () => podcastsComponent.getPodcasts());
        break;
      case 'podcast-detail':
        viewPipe.loadView('/app/views/podcast.component.html', ViewPipe.VIEW_MAIN, () => podcastComponent.getPodcast(event.state.podcastId));
        break;
      case 'episode-detail':
        viewPipe.loadView('/app/views/episode.component.html', ViewPipe.VIEW_DETAIL, () => episodeComponent.getEpisode(event.state.episodeTitle, event.state.episodeDescription, event.state.episodeUrl));
        break;
    }
  }
}

// onDOMReady
document.addEventListener('DOMContentLoaded', function (event) {
  let data = {
    name: 'podcasts-list'
  };
  history.replaceState(data, document.title, document.location.href);
  init();
}, false);

// onClick
document.addEventListener('click', function (event) {
  event.preventDefault();
  // Podcasts list
  let podcastsListItem = event.target.closest('[data-router="podcasts-list"]');
  if (event.target && podcastsListItem) {
    let data = {
      name: 'podcasts-list'
    };
    let title = 'Podcasts list';
    let url = '/';
    history.pushState(data, title, url);
    viewPipe.loadView('/app/views/podcasts.component.html', ViewPipe.VIEW_MAIN, () => podcastsComponent.getPodcasts());
  }
  // Podcast detail
  let podcastItem = event.target.closest('[data-router="podcast-detail"]');
  if (event.target && podcastItem) {
    let podcastId = podcastItem.dataset.podcastId;
    let data = {
      name: 'podcast-detail',
      podcastId: podcastId
    };
    let title = 'Podcast detail';
    let url = `/podcast/${podcastId}`;
    history.pushState(data, title, url);
    viewPipe.loadView('/app/views/podcast.component.html', ViewPipe.VIEW_MAIN, () => podcastComponent.getPodcast(podcastId));
  }
  // Episode detail
  let episodeItem = event.target.closest('[data-router="episode-detail"]');
  if (event.target && episodeItem) {
    let podcastId = episodeItem.dataset.podcastId;
    let episodeId = episodeItem.dataset.episodeId;
    let episodeTitle = episodeItem.dataset.episodeTitle;
    let episodeDescription = episodeItem.dataset.episodeDescription;
    let episodeUrl = episodeItem.getAttribute('href');
    let data = {
      name: 'episode-detail',
      episodeTitle: episodeTitle,
      episodeDescription: episodeDescription,
      episodeUrl: episodeUrl
    };
    let title = 'Episode detail';
    let url = `/podcast/${podcastId}/episode/${episodeId}`;
    history.pushState(data, title, url);
    viewPipe.loadView('/app/views/episode.component.html', ViewPipe.VIEW_DETAIL, () => episodeComponent.getEpisode(episodeTitle, episodeDescription, episodeUrl));
  }
});

// Functions

function init () {
  viewPipe.loadView('/app/views/podcasts.component.html', ViewPipe.VIEW_MAIN, () => podcastsComponent.getPodcasts());
}

export class AppRouter {}
