/* global fetch */

const CORS_PROXY = 'https://cors-anywhere.herokuapp.com/';

export class RequestService {
  static get CORS_PROXY () {
    return CORS_PROXY;
  }

  getPodcasts (url) {
    return fetch(CORS_PROXY + url);
  }

  getPodcast (url, podcastId) {
    return fetch(CORS_PROXY + url + podcastId);
  }

  getPodcastTracklist (url) {
    return fetch(CORS_PROXY + url);
  }
}
