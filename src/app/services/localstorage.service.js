/* global localStorage */

export class LocalStorageService {
  getItem (key) {
    return localStorage.getItem(key);
  }

  setItem (key, value) {
    localStorage.setItem(key, value);
  }
}
