const NOW = Date.now();
const ONE_DAY = 1000 * 60 * 60 * 24;

export class DatePipe {
  static get NOW () {
    return NOW;
  }

  static get ONE_DAY () {
    return ONE_DAY;
  }

  formatDateToLocale (date, locale) {
    return new Intl.DateTimeFormat(locale).format(new Date(date));
  }

  isOneDayOld (date) {
    let dateTimeStamp = new Date(Number(date)).getTime();
    if (dateTimeStamp) {
      // Compare timestamps
      let timePassed = (DatePipe.NOW - dateTimeStamp);
      return timePassed > DatePipe.ONE_DAY;
    }
  }

  secondsToTimeString (seconds) {
    return Math.floor(seconds / 60) + ':' + (seconds % 60 ? seconds % 60 : '00');
  }
}
