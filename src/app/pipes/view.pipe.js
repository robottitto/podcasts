/* global DOMParser, fetch */

const DEFAULT_LOCALE = 'es-ES';
const PRELOADER = '[data-property="preloader"]';
const VIEW_DETAIL = '[data-view="detail"]';
const VIEW_MAIN = '[data-view="main"]';

export class ViewPipe {
  static get DEFAULT_LOCALE () {
    return DEFAULT_LOCALE;
  }

  static get PRELOADER () {
    return PRELOADER;
  }

  static get VIEW_DETAIL () {
    return VIEW_DETAIL;
  }

  static get VIEW_MAIN () {
    return VIEW_MAIN;
  }

  hide (element) {
    document.querySelector(element).hidden = true;
  }

  show (element) {
    document.querySelector(element).hidden = false;
  }

  loadView (template, section, callback) {
    this.show(ViewPipe.PRELOADER);
    let view = fetch(template);
    view.then((response) => response.text()).then((html) => {
      let parser = new DOMParser();
      let template = parser.parseFromString(html, 'text/html');
      let newHtml = template.querySelector(section).outerHTML;
      document.querySelector(section).outerHTML = newHtml;
      callback();
      // hide(PRELOADER);
    }).catch(err => console.error(err));
  }
}
