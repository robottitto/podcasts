import { LocalStorageService } from '../services/localstorage.service.js';
import { RequestService } from '../services/request.service.js';
import { DatePipe } from '../pipes/date.pipe.js';
import { ViewPipe } from '../pipes/view.pipe.js';
let localStorageService = new LocalStorageService();
let requestService = new RequestService();
let datePipe = new DatePipe();
let viewPipe = new ViewPipe();

const PODCAST_URL = 'https://itunes.apple.com/lookup?id=';

export class PodcastComponent {
  static get PODCAST_URL () {
    return PODCAST_URL;
  }

  getPodcast (id) {
    let lastRequestDate = localStorageService.getItem('lastRequestDatePodcast' + id);
    let json, image, name, author, trackId, trackCount, feedUrl;
    if (lastRequestDate && !datePipe.isOneDayOld(lastRequestDate)) {
      json = JSON.parse(localStorageService.getItem('podcast' + id));
      image = json.results[0].artworkUrl100;
      name = json.results[0].collectionName;
      author = json.results[0].artistName;
      trackId = json.results[0].trackId;
      trackCount = json.results[0].trackCount;
      feedUrl = json.results[0].feedUrl;
      this.loadPodcastDetail(image, name, author, trackId, trackCount, feedUrl);
    } else {
      let podcast = requestService.getPodcast(PODCAST_URL, id);
      podcast.then((response) => response.json()).then((json) => {
        console.log(json);
        if (json.resultCount) {
          image = json.results[0].artworkUrl100;
          name = json.results[0].collectionName;
          author = json.results[0].artistName;
          trackId = json.results[0].trackId;
          trackCount = json.results[0].trackCount;
          feedUrl = json.results[0].feedUrl;
          this.loadPodcastDetail(image, name, author, trackId, trackCount, feedUrl);
          // Save in browser
          localStorageService.setItem('lastRequestDatePodcast' + id, DatePipe.NOW);
          localStorageService.setItem('podcast' + id, JSON.stringify(json));
        }
      }).catch(err => console.error(err));
    }
  }

  loadPodcastDetail (image, name, author, trackId, trackCount, feedUrl) {
    let podcastTracklist = requestService.getPodcastTracklist(feedUrl);
    podcastTracklist.then((response) => response.text()).then((xml) => {
      xml = new DOMParser().parseFromString(xml, 'text/xml');
      console.log(xml);
      let description = xml.querySelector('description').textContent;
      document.querySelector('.podcast-info').dataset.podcastId = trackId;
      document.querySelector('.podcast-info').setAttribute('href', `/podcast/${trackId}`);
      document.querySelector('.podcast-image').setAttribute('src', image);
      document.querySelector('.podcast-name').textContent = name;
      document.querySelector('.podcast-author .field-value').textContent = author;
      document.querySelector('.podcast-description .field-value').innerHTML = description;
      document.querySelector('.episodes-count .field-value').textContent = trackCount;
      let episodes = xml.querySelectorAll('item');
      let tbody = document.querySelector('#episodesList table tbody');
      episodes.forEach((episode) => {
        let id = episode.querySelector('guid').textContent;
        let url = episode.querySelector('enclosure').getAttribute('url');
        let title = episode.querySelector('title').textContent;
        let description = '';
        if (episode.querySelector('description')) {
          description = episode.querySelector('description').textContent;
        }
        let date = episode.querySelector('pubDate').textContent;
        date = datePipe.formatDateToLocale(date, datePipe.DEFAULT_LOCALE);
        let duration = episode.getElementsByTagNameNS('http://www.itunes.com/dtds/podcast-1.0.dtd', 'duration')[0].textContent;
        if (!duration.includes(':')) {
          duration = datePipe.secondsToTimeString(duration);
        }
        let episodeTemplate = document.querySelector('#episodeTemplate');
        let td = episodeTemplate.content.querySelectorAll('td');
        td[0].querySelector('.episode-link').textContent = title;
        td[0].querySelector('.episode-link').dataset.podcastId = trackId;
        td[0].querySelector('.episode-link').dataset.episodeId = id;
        td[0].querySelector('.episode-link').dataset.episodeTitle = title;
        td[0].querySelector('.episode-link').dataset.episodeDescription = description;
        td[0].querySelector('.episode-link').setAttribute('href', url);
        td[1].textContent = date;
        td[2].textContent = duration;
        let clone = document.importNode(episodeTemplate.content, true);
        tbody.appendChild(clone);
      });
      viewPipe.hide(ViewPipe.PRELOADER);
    }).catch(err => console.error(err));
  }
}
