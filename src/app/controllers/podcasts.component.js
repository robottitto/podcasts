import { LocalStorageService } from '../services/localstorage.service.js';
import { RequestService } from '../services/request.service.js';
import { DatePipe } from '../pipes/date.pipe.js';
import { ViewPipe } from '../pipes/view.pipe.js';
let localStorageService = new LocalStorageService();
let requestService = new RequestService();
let datePipe = new DatePipe();
let viewPipe = new ViewPipe();

const LAST_PODCASTS = 'lastPodcasts';
const LAST_REQUEST_DATE = 'lastRequestDate';
const PODCASTS_URL = 'https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json';

export class PodcastsComponent {
  static get LAST_PODCASTS () {
    return LAST_PODCASTS;
  }
  static get LAST_REQUEST_DATE () {
    return LAST_REQUEST_DATE;
  }
  static get PODCASTS_URL () {
    return PODCASTS_URL;
  }

  getPodcasts () {
    let podcasts;
    let lastRequestDate = localStorageService.getItem('LAST_REQUEST_DATE');
    // Get the list of the 100 most popular podcasts (from local storage or internet)
    if (lastRequestDate && !datePipe.isOneDayOld(lastRequestDate)) {
      podcasts = JSON.parse(localStorageService.getItem('LAST_PODCASTS'));
      console.log(podcasts);
      podcasts.feed.entry.forEach((podcast) => {
        this.addPodcast(podcast);
      });
      this.updateCounter(podcasts.feed.entry.length);
      viewPipe.hide(ViewPipe.PRELOADER);
    } else {
      podcasts = requestService.getPodcasts(PODCASTS_URL);
      podcasts.then((response) => response.json()).then((json) => {
        // Get the list of the 100 most popular podcasts
        console.log(json);
        json.feed.entry.forEach((podcast) => {
          this.addPodcast(podcast);
        });
        // Save in browser
        localStorageService.setItem(LAST_REQUEST_DATE, datePipe.NOW);
        localStorageService.setItem(LAST_PODCASTS, JSON.stringify(json));
        // Update counter
        this.updateCounter(json.feed.entry.length);
        viewPipe.hide(ViewPipe.PRELOADER);
      }).catch(err => console.error(err));
    }
  }

  addPodcast (podcast) {
    let id = podcast.id.attributes['im:id'];
    let image = podcast['im:image'][2].label;
    let name = podcast['im:name'].label;
    let author = podcast['im:artist'].label;
    let podcasts = document.querySelector('#podcasts');
    let podcastTemplate = document.querySelector('#podcastTemplate');
    let podcastItem = podcastTemplate.content.querySelector('.podcast-item');
    podcastItem.setAttribute('href', '/podcast/' + id);
    podcastItem.dataset.podcastId = id;
    let podcastImage = podcastTemplate.content.querySelector('.podcast-image');
    podcastImage.setAttribute('src', image);
    let podcastName = podcastTemplate.content.querySelector('.podcast-name');
    podcastName.textContent = name;
    let podcastAuthor = podcastTemplate.content.querySelector('.podcast-author');
    podcastAuthor.textContent = author;
    let clone = document.importNode(podcastTemplate.content, true);
    podcasts.appendChild(clone);
  }

  filterPodcasts (q) {
    let count = 0;
    q = q.toLowerCase();
    let podcasts = JSON.parse(localStorageService.getItem(LAST_PODCASTS));
    podcasts.feed.entry.forEach((podcast) => {
      let name = podcast['im:name'].label.toLowerCase();
      let artist = podcast['im:artist'].label.toLowerCase();
      if (name.includes(q) || artist.includes(q)) {
        this.addPodcast(podcast);
        count++;
      }
    });
    this.updateCounter(count);
  }

  updateCounter (count) {
    let counter = document.querySelector('#counter');
    counter.textContent = count;
  }
}

document.addEventListener('keyup', function (event) {
  if (event.target && event.target.matches('#filterPodcasts')) {
    let podcasts = document.querySelectorAll('.podcast-item');
    podcasts.forEach((podcast) => {
      podcast.parentNode.removeChild(podcast);
    });
    let podcastsComponent = new PodcastsComponent();
    let q = event.target.value;
    podcastsComponent.filterPodcasts(q);
  }
});
