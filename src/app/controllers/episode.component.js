import { ViewPipe } from '../pipes/view.pipe.js';
let viewPipe = new ViewPipe();

export class EpisodeComponent {
  getEpisode (title, description, url) {
    document.querySelector('.episode-title').textContent = title;
    document.querySelector('.episode-description').innerHTML = description;
    document.querySelector('#episodePlayer').setAttribute('src', url);
    document.querySelector('#episodePlayer').load();
    viewPipe.hide(ViewPipe.PRELOADER);
  }
}
