import { AppRouter } from './app/app.router.js';

const DEBUG_MODE = true;

// Debug logs
if (typeof (console) === 'undefined') {
  console = {};
}
if (!DEBUG_MODE || typeof (console.log) === 'undefined') {
  console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = function () { };
}
